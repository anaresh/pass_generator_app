defmodule PassGeneratorAppTest do
  use ExUnit.Case

  setup do
    options = %{"length" => "5",
                "numbers" => "false",
                "uppercase" => "false",
                "symbols" => "false"}

    options_type = %{
        lowercase: Enum.map(?a..?z, fn(x) -> <<x>> end),
        numbers: Enum.map(0..9, fn(x) -> Integer.to_string(x) end),
        uppercase: Enum.map(?A..?Z, fn(x) -> <<x>> end),
        symbols: String.split("~!@#$%^&*()_-+={}[]:;'|<>,.?|","", trim: true)
        }
    %{options: options, options_type:  options_type}
  end

  test "test result is a string", %{options: options} do
    {:ok, result} = PassGeneratorApp.generate(options)
    assert is_bitstring(result)
  end

  test "returns an error back when no length is given" do
    options = %{"invalid" => "false"}

    assert {:error, _error} = PassGeneratorApp.generate(options)
  end

  test "returns an error when length is non-int" do
    options = %{"length" => "false"}

    assert {:error, _error} = PassGeneratorApp.generate(options)
  end

  test "test the length of generated password" do
    options = %{"length" => "10"}
    {:ok, result} = PassGeneratorApp.generate(options)

    assert 10 == String.length(result)
  end

  test "returns a lowercase string when only length is given", %{options_type: ot} do
    options = %{"length" => "10"}
    {:ok, result} = PassGeneratorApp.generate(options)

    assert String.contains?(result, ot.lowercase)
    refute String.contains?(result, ot.numbers)
    refute String.contains?(result, ot.uppercase)
    refute String.contains?(result, ot.symbols)
  end

  test "Returns error when Options Value is non-booleans" do
    options = %{
      "length" => "10",
      "numbers" => "invalid",
      "symbols" => "0",
      "uppercase" => "false"
      }

    assert {:error, _error} = PassGeneratorApp.generate(options)
  end

  test "Returns error when invalid  option is supplied" do
    options = %{
      "length" => "10",
      "invalid" => "false",
      }

    assert {:error, _error} = PassGeneratorApp.generate(options)
  end

  test "returns uppercase string in result", %{options_type: ot} do
    options = %{
      "length" => "10",
      "numbers" => "false",
      "uppercase" => "true",
      "symbols" => "false"
    }
    {:ok, result} = PassGeneratorApp.generate(options)
    assert String.contains?(result, ot.uppercase)
    refute String.contains?(result, ot.numbers)
    refute String.contains?(result, ot.symbols)
  end

  test "returns numbers string in result", %{options_type: ot} do
    options = %{
      "length" => "10",
      "numbers" => "true",
      "uppercase" => "false",
      "symbols" => "false"
    }
    {:ok, result} = PassGeneratorApp.generate(options)
    refute String.contains?(result, ot.uppercase)
    assert String.contains?(result, ot.numbers)
    refute String.contains?(result, ot.symbols)
  end

    test "returns symbols string in result", %{options_type: ot} do
    options = %{
      "length" => "10",
      "numbers" => "false",
      "uppercase" => "false",
      "symbols" => "true"
    }
    {:ok, result} = PassGeneratorApp.generate(options)
    refute String.contains?(result, ot.uppercase)
    refute String.contains?(result, ot.numbers)
    assert String.contains?(result, ot.symbols)
  end

  test "returns uppercase and numbers string in result", %{options_type: ot} do
    options = %{
      "length" => "10",
      "numbers" => "true",
      "uppercase" => "true",
      "symbols" => "false"
    }
    {:ok, result} = PassGeneratorApp.generate(options)
    assert String.contains?(result, ot.uppercase)
    assert String.contains?(result, ot.numbers)
    refute String.contains?(result, ot.symbols)
  end

  test "returns uppercase, lowercase, numbers and numbers string in result", %{options_type: ot} do
    options = %{
      "length" => "10",
      "numbers" => "true",
      "uppercase" => "true",
      "symbols" => "true"
    }
    {:ok, result} = PassGeneratorApp.generate(options)
    assert String.contains?(result, ot.uppercase)
    assert String.contains?(result, ot.numbers)
    assert String.contains?(result, ot.symbols)
  end
end
