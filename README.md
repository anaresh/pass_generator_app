# PassGeneratorApp

Password Generator backend Elixir Application

## Clone and Test
You can clone the app using git

git clone https://gitlab.com/anaresh/pass_generator_app.git
cd pass_generator_app
mix test

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `pass_generator_app` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:pass_generator_app, "~> 0.1.0"}
  ]
end
```

OR clone from git and use PassGeneratorApp.generate 

```elixir
def deps do
  [
    {:pass_generator_app, git: "https://gitlab.com/anaresh/pass_generator_app.git", branch: "main"}
  ]
end
```


