defmodule PassGeneratorApp do
  @moduledoc """
  PasswordGenerator helps to generate password based on the requirements.
  It accepts map of "lenght", "uppercase", "lowercase" and "symbol".
    where "length" is blablqabla
  """

  @doc """
  Generate password

  ## Examples

      iex> PasswordGenerator.generate(%{"length" => "5", "lowercase" => "false", "symbol" => "false", "uppercase" => "false"})
      "abcde"

  """

  @spec generate(options :: map()) :: {:ok, bitstring()} | {:error, bitstring()}
  def generate(options) do
    length = Map.has_key?(options, "length")
    validate_length(length, options)
  end

  defp validate_length(:false, _options), do: {:error, "Please provide length option"}
  defp validate_length(:true, options) do
    length = options["length"]
    numbers = Enum.map(0..9, & Integer.to_string(&1))
    len_list = String.split(length, "", trim: true)
    length = Enum.all?(len_list, & Enum.member?(numbers, &1))
    validate_length_is_int(length, options)
  end

  defp validate_length_is_int(false, _options), do: {:error, "Invalid length option value"}
  defp validate_length_is_int(true, options) do
    length = options["length"] |> String.trim() |> String.to_integer()
    options_without_len = Map.delete(options, "length")
    op_values = Map.values(options_without_len)
    values = Enum.all?(op_values, & &1 |> String.to_atom() |> is_boolean())
    validate_options_val_are_boolean(values, length, options_without_len)
  end

  defp validate_options_val_are_boolean(false, _length, _options_without_len) do
    {:error, "Only booleans are allowed as options"}
  end
  defp validate_options_val_are_boolean(true, length, options) do
    options_keys = Map.keys(options)
    options = included_options(options)
    valid_keys = ["uppercase", "numbers", "symbols"]
    op_keys = Enum.all?(options_keys, & Enum.member?(valid_keys, &1))
    validate_op_keys(op_keys, length, options)
  end

  defp validate_op_keys(false, _len, _op), do: {:error, "Invalid option key"}
  defp validate_op_keys(true, length, options) do
    generate_string(length, options)
  end

  defp generate_string(length, options) do
    options = [:lowercase_letter | options]
    string = Enum.map(1..length, fn(_)-> get(Enum.random(options)) end)
             |> Enum.shuffle()
             |> List.to_string()
    {:ok, string}
  end

  defp get(:lowercase_letter), do: <<Enum.random(?a..?z)>>
  defp get(:numbers), do: Enum.random(0..9) |> Integer.to_string()
  defp get(:uppercase), do: <<Enum.random(?A..?Z)>>
  defp get(:symbols) do
    String.split("~!@#$%^&*()_-+={}[]:;'|<>,.?|", "", trim: true) |> Enum.random()
  end

  defp included_options(options) do
    true_options = Enum.filter(options, fn({_, val}) -> val |> String.to_atom() end)
    Enum.map(true_options, fn({key, _}) -> String.to_atom(key) end)
  end
end
